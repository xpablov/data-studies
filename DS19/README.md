# Data Studies 2019 @ ddinf Aarhus 
![](images/bike.png)

[LECTURE](#lecture) // [WORKSHOPS](#workshops) // [LINKS FOR THE COURSE](#links-for-the-course) // [TASKS](#tasks) // [FINAL PROJECT](#final-project) // [EXAM](#exam) // [CALENDAR](#calendar) // [USEFUL LINKS](#useful-links)    

### Topics and slides:  
:fire: [S01](/S01) // [S02](/S02) // [S03](S03) // [S04](S04) // [S05](S05) // [S06](S06) // [S07](S07) // [S08](S08) // [S09](S09) // [S10](S10) // [S11](S11) // [S12](S12) // S13 // S14

### LECTURE
**Time:** Thursdays: 11.00-14.00  
**Location:** 5510-104 (Lille Auditorium)  
**Instructor:** Pablo Velasco // pvelasco@cc.au.dk // http://pablov.me/  
Office: 5347 (Wiener building) room 117

### WORKSHOPS
* :hamster:  
Thursdays 16.00-18.00 (5008-131) [W36, 40, 43]  
Fridays 10.00-12.00 (5008-127) [W37, 39, 41, 45-50]  
Fridays 10.00-12.00 (5008-131) [W38]  
Fridays 10.00-12.00 (5335-295) [W44]  
* :koala:  
Fridays 8.00-10.00 (5008-140)  
* :panda_face:  
Fridays 13.00-15.00 (5008-140)  

**Instructor:** Rasmus Raspel //  201803326@post.au.dk  
**Instructor:** Maximilian Schlüter //  mschlueter@cc.au.dk  
**Instructor:** Nathalia Novais //  201702970@post.au.dk

Data Studies explores different contexts where data is gathered and produced. The course considers the question of what can be understood as “data” from these contexts, and how this understanding changes the perspective of data regarding its collection, analysis, uses, and methodological approaches. While theory and historical knowledge is provided, the course aims to generate reflection from the students based on practices and a diversity of uses of data. However, the approach to data is guided by social science’s concerns.

**No materials are strictly required for the course, however you will need**:

* To create an account in Gitlab (put your username in the shared pad)
* You will need an installation of an [Anaconda Python](https://www.anaconda.com/distribution/) environment for some parts of the course. 

### LINKS FOR THE COURSE:
* Gitlab Course home: https://gitlab.com/xpablov/data-studies  
* Shared Pad: https://pad.riseup.net/p/Data-Studies2019-keep  
* Readings (files): https://drive.google.com/drive/folders/19ZRv_O1wZMh3iZywAA-70uLwF0qnx6J2?usp=sharing  

### TASKS/DELIVERABLES (for every block)

* **Mini-project groupwork**
    * Mini-projects groupwork will take place all along the term. You will have to use specific tools for each mini-project. **Short presentations of the mini-projects will take place during the lecture on weeks 38, 40, 44, 46, and 48**. Each group will only present once. 
* **Gitlab Wikis**
    * You are expeced to work on the wikis during the block (don't leave it to the last minute!). ~~Deadlines for submitting both the wiki-report and the wiki-thoughts are at the end of each block: Tuesdays 11.59pm on weeks 39, 41, 45, 47 and 49~~   
        * *Wiki-report*
            * All groups should generate a report on their groupwork mini-projects on the gitlab wiki. **The deadlines for submitting the wiki-report are Tuesdays 11.59pm of presentation weeks: 17-sep, 01-oct, 29-oct, 12-nov, 26-nov.**
        * *Wiki-thoughts*
            * Everyone (individual work) should write a reflection on the readings of the block, in the gitlab wiki. The wiki-thoughts are not meant to be a summary of the readings, but your own ideas/reflections based on them. They may be written in first person, but an academic style is recommended. **The deadlines for submitting the wiki-thoughts are Tuesdays 11.59pm at the beginning of new blocks: 24-sep, 08-oct, 05-nov, 19-nov, 03-dec.**
* **Peer Review** (for all deliverables)
    * Students from groups that are not presenting should make comments on their peers wikis (report OR theory). Groups that are presenting do not need to comment on other wikis.

### FINAL PROJECT
The project is a groupwork, which must use the main TCAT database, alongside with other tools and methods seen in the course. More information on the final project can be found in the [Research Project page](research_project.md). Week 41 will be focused on how to design the final project, which you will develop in your groups from week 41 to week 48. **A final presentation of the project will take place on week 49**.

### EXAM
The exam is a free home assignment where the student analyzes and evaluates independently selected cases using the course's theories, concepts and methods. **The exam should also include a synopsis of the final project (10-12 standard pages)**.

**EXTENDED INFORMATION ON THE EXAM HERE**: https://gitlab.com/xpablov/data-studies/blob/master/DS19/Exam.md  

## CALENDAR



| Week | Date   | Block                             | Session Readings |                               
| :---:| :-----:|-------------------------------------|--------------------
| [36](S01)   | 05-sep | 0. ETHOS                            | ☛ Rogers, Richard. “Foundations of Digital Methods.” In The Datafied Society. Studying Culture through Data, 75–94. Amsterdam University Press, 2017. (20 p.) <br> ☛ Annette N. Markham. “Research Ethics in Context.” In The Datafied Society. Studying Culture through Data, edited by van Es Karin and Schäfer Mirko Tobias, 201–9. Amsterdam University Press, 2017. (19 p.)  
| [37](S02)  | 12-sep | 1. DATA CAPTURE                     | ☛ Gitelman, Lisa. Raw Data Is an Oxymoron. MIT Press, 2013. (chapter 1: Data before the fact, 25 p.) <br> ☛ Daly, Angela, Kate Devitt, and Monique Mann, eds. Good Data. Theory on Demand 29. Amsterdam: Institute of Network Cultures, 2019. (Chapter 10: Making Data Public? The Open Data Index as Participatory Device, 15 p.) 
| [38](S03)   | 19-sep |                                     | ☛ Beer, David. The Data Gaze: Capitalism, Power and Perception. 1 edition. Thousand Oaks, CA: SAGE Publications Ltd, 2018. (Chapter 5: the diagnostic eye, 30 p.) <br>☛ Ruppert, Evelyn. “Category.” In Inventive Methods: The Happening of the Social, edited by Celia Lury and Nina Wakeford, 36–47. Routledge, 2012. (11 p.)
| [39](S04)   | 26-sep | 2. PLATFORMS, MAPS & BEAUTIFUL DATA | ☛ [“Focal Point: Harvard Professor Steven Pinker Says the Truth Lies in the Data.”](https://news.harvard.edu/gazette/story/2019/06/focal-point-harvard-professor-steven-pinker-says-the-truth-lies-in-the-data/) Harvard Gazette (blog), June 21, 2019. <br> ☛ Hochman, Nadav, and Lev Manovich. [“Zooming into an Instagram City: Reading the Local through Social Media.”](http://firstmonday.org/ojs/index.php/fm/article/view/4711) First Monday 18, no. 7 (June 17, 2013). (~50 p.)
| [40](S05)  | 03-oct |                                     | ☛ Malik, Momin M. “Identifying Platform Effects in Social Media Data,” n.d., 9. (9 p.) [ignore equations!] <br> ☛ Lury, Celia, Rachel Fensham, Alexandra Heller-Nicholas, Sybille Lammes, Angela Last, Mike Michael, and Emma Uprichard, eds. Routledge Handbook of Interdisciplinary Research Methods. 1st ed. Routledge, 2018. (Section 2, Chapter 12: visualizing data, 10 p.)
| [41](S06)   | 10-oct | ☼                                   | **Project session** <br> ☛ Ruppert, Evelyn, John Law, and Mike Savage. “Reassembling Social Science Methods: The Challenge of Digital Devices.” Theory, Culture & Society 30, no. 4 (2013): 22–46. (24 p.) <br> ☛ Marres, Noortje. “The Redistribution of Methods: On Intervention in Digital Social Research, Broadly Conceived.” The Sociological Review 60 (2012): 139–65. (26 p.)
| 42   | 17-oct | ⮾                                  | \* no class
| [43](S07)   | 24-oct | 3. METHODS AND NETWORKS             | ☛ Venturini, Tommaso, Liliana Bounegru, Mathieu Jacomy, and Jonathan Gray. 2017. ‘How to tell stories with networks’. In The Datafied Society. Studying Culture through Data, 155–69. Amsterdam University Press. (14 p.) <br> ☛ Bruns, Axel, and Jean Burgess. “The Use of Twitter Hashtags in the Formation of Ad Hoc Publics,” n.d., 9. (9 p.)<br> ☛ Page, L., Brin, S., Motwani, R., & Winograd, T. (1999). The PageRank citation ranking: Bringing order to the web. Stanford InfoLab. (17 p.) [ignore equations!]<br> ☛ (**highly recommended**) Venturini, Tommaso, and Mathieu Jacomy. “Visual Network Analysis.” Sciences Po Médialab Working Papers.
| [44](S08)   | 31 oct |                                     | ☛ Latour, Bruno, Pablo Jensen, Tommaso Venturini, Sébastian Grauwin, and Dominique Boullier. 2012. ‘“The Whole Is Always Smaller than Its Parts”–a Digital Test of Gabriel Tardes’ Monads’. The British Journal of Sociology 63 (4): 590–615. (25 p.) <br> ☛ Marres, Noortje. 2015. ‘Why Map Issues? On Controversy Analysis as a Digital Method’. Science, Technology & Human Values, 0162243915574602. (24 p.)
| [45](S09)   | 07-nov | 4. BIG DATA, BIG POLITICS           | ☛ Uprichard, Emma. [“Focus: Big Data, Little Questions?”](https://discoversociety.org/2013/10/01/focus-big-data-little-questions/) Discover Society, October 1, 2013. (6 p.) <br> ☛ boyd, danah, and Kate Crawford. “Critical Questions For Big Data: Provocations for a Cultural, Technological, and Scholarly Phenomenon.” Information, Communication & Society 15, no. 5 (June 2012): 662–79. (18 p.)
| [46](S10)   | 14-nov |                                     | ☛ Dijck, Jose van. 2014. ‘Datafication, Dataism and Dataveillance: Big Data between Scientific Paradigm and Ideology’. Surveillance & Society 12 (2): 197–208. (11 p.) <br> ☛ ~~Kitchin, Rob, and Tracey P Lauriault. “Towards Critical Data Studies: Charting and Unpacking Data Assemblages and Their Work,” 2014, 19. (19 p.)~~ <br> ☛ Cathy, O’Neil. Weapons of Math Destruction: How Big Data Increases Inequality and Threatens Democracy. 1 edition. London: Penguin, 2017. (Chapter 10, 18 p.)
| [47](S11)   | 21-nov | 5. DATA LOGIC & RESISTANCE TACTICS  | **Choose two (all are interesting!):**<br>☛ **A.** Agre, Philip E. 1994. ‘Surveillance and Capture: Two Models of Privacy’. The Information Society 10 (2): 101–27. <br>☛ **B.** Pasquinelli, Matteo, ed. Alleys of Your Mind. Lüneburg: meson press, 2015. (Chapter 1: Texeira, Ana, The pigeon in the machine, 14 p.) <br>☛ **C.** Hear (podcast) Reply All, The Crime Machine ([part I](https://gimletmedia.com/shows/reply-all/o2hx34/) and [part II](https://gimletmedia.com/shows/reply-all/n8hwl7))
| [48](S12)   | 28-nov |                                     | **There are no required readings for this session. The following materials are recommended**:<br>☛ Colakides, Yiannis. State Machines: Reflections and Actions at the Edge of Digital Citizenship, Finance, and Art. Amsterdam: Institute of Network Cultures, 2019. (Chapter: “There is no anonymity in the database”, 12 p.) <br> ☛ Daly, Angela, Kate Devitt, and Monique Mann, eds. Good Data. Theory on Demand 29. Amsterdam: Institute of Network Cultures, 2019. (Chapter 14: Data for the Social Good: Toward a Data-Activist Research Agenda, 16 p.) <br> ☛ (Podcast) Reply All, [The Prophet](https://gimletmedia.com/shows/reply-all/j4hl36)
| 49   | 05-dec | ☼                                   | **Presentations on final project**<br><br>**Recommended reading for all groups**:<br>Venturini, Tommaso, Liliana Bounegru, Jonathan Gray, and Richard Rogers. “A Reality Check(List) for Digital Methods.” New Media & Society, April 20, 2018, 146144481876923.
| 50   | 12-dec | ☼                                  | **Ways of seeing with computer vision**<br>Guest lecture and short film with Gabriel Pereira
| 02   | 06-jan | ☼                                   | **Exam submission**

### USEFUL LINKS
#### Digital Methods
* Digital Methods Initiative: https://digitalmethods.net  
* Mapping Online Publics Online: http://mappingonlinepublics.net/  
* Issuemapping: http://issuemapping.net/  
* Science Po medialab: https://medialab.sciencespo.fr/tools/  
* Mapping controversies: http://mappingcontroversies.net (offline, but accessible through the internet archive’s wayback machine)
* Open Data Index: https://index.okfn.org/
#### Datasets
* Harvard Dataverse: https://dataverse.org/
* TWITTER: https://about.twitter.com/en_us/values/elections-integrity.html#data
* FACEBOOK: https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/EIAACS
* ACADEMIC: http://academictorrents.com/
* GLOBAL (worldbank): https://databank.worldbank.org/
* US: https://catalog.data.gov/dataset
* EUROPE: https://www.europeandataportal.eu/data/en/dataset
* DENMARK (statistics): https://www.statbank.dk
* GOOGLE: https://toolbox.google.com/datasetsearch
* AWS opendata: https://registry.opendata.aws/
* CONCEPTNET: http://conceptnet.io/
* CORPORA: small corpuses: https://github.com/dariusk/corpora
* OPENDOAR: open access repositories: http://v2.sherpa.ac.uk/opendoar/
* other lists: https://www.kdnuggets.com/datasets/index.html
#### Coding
* Codecademy: https://www.codecademy.com/learn
* ReplIt: https://repl.it/ also online python editor: https://repl.it/languages/python3
* Dataquest: https://www.dataquest.io/
#### Podcasts
* Data Stories https://datastori.es/             
* Note to Self https://www.wnycstudios.org/podcasts/notetoself
* Reply All https://gimletmedia.com/shows/reply-all

