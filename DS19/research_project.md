# Research Project

**Asking the right questions**

## Paste your research project as usual in these wiki entries:  
**Hamsters**: https://gitlab.com/xpablov/data-studies/wikis/H-RP  
**Koalas**: https://gitlab.com/xpablov/data-studies/wikis/K-RP  
**Pandas**: https://gitlab.com/xpablov/data-studies/wikis/P-RP  

## 42
The "Answer to the Ultimate Question of Life, the Universe, and Everything"
Calculated over a period of 7.5 million years 
(*The Hitch Hiker’s Guide to the Galaxy*)

> “O Deep Thought Computer,” he said, “the task we have designed you to perform is
this. We want you to tell us...” he paused, “... the Answer!”  
“The answer?” said Deep Thought. “The answer to what?”  
“Life!” urged Fook.  
“The Universe!” said Lunkwill.  
“Everything!” they said in chorus.  
Deep Thought paused for a moment’s reflection.  
“Tricky,” he said finally.  
“But can you do it?”  
Again, a significant pause.  
“Yes,” said Deep Thought, “I can do it.”  
“There is an answer?” said Fook with breathless excitement.”  
“A simple answer?” added Lunkwill.  
“Yes,” said Deep Thought. “Life, the Universe, and Everything. There is an answer.  
But,” he added, “I’ll have to think about it.”  
>
>(...)
>
>“Er... Good morning, O Deep Thought,” said Loonquawl nervously, “do you have...
er, that is...”  
“An answer for you?” interrupted Deep Thought majestically. “Yes. I have.”  
The two men shivered with expectancy. Their waiting had not been in vain.  
“There really is one?” breathed Phouchg.  
“There really is one,” confirmed Deep Thought.  
“To Everything? To the great Question of Life, the Universe and Everything?”  
“Yes.”  
Both of the men had been trained for this moment, their lives had been a preparation
for it, they had been selected at birth as those who would witness the answer, but even so they found themselves gasping and squirming like excited children.  
“And you’re ready to give it to us?” urged Loonquawl.  
“I am.”  
“Now?”  
“Now,” said Deep Thought.  
They both licked their dry lips.  
“Though I don’t think,” added Deep Thought, “that you’re going to like it.”  
“Doesn’t matter!” said Phouchg. “We must know it! Now!”  
“Now?” inquired Deep Thought.  
“Yes! Now...”  
“Alright,” said the computer and settled into silence again. The two men fidgeted. The
tension was unbearable.  
“You’re really not going to like it,” observed Deep Thought.  
“Tell us!”  
“Alright,” said Deep Thought. “The Answer to the Great Question...”  
“Yes...!”  
“Of Life, the Universe and Everything...” said Deep Thought.  
“Yes...!”  
“Is...” said Deep Thought, and paused.  
“Yes...!”  
“Is...”  
“Yes...!!!...?”  
“Forty-two,” said Deep Thought, with infinite majesty and calm.  
>
>(...)
>
>“Forty-two!” yelled Loonquawl. “Is that all you’ve got to show for seven and a half
million years’ work?”  
“I checked it very thoroughly,” said the computer, “and that quite definitely is the
answer. I think the problem, to be quite honest with you, is that you’ve never actually
known what the question is.”  
“But it was the Great Question! The Ultimate Question of Life, the Universe and
Everything!” howled Loonquawl.  
“Yes,” said Deep Thought with the air of one who suffers fools gladly, “but what
actually is it?”  
A slow stupefied silence crept over the men as they stared at the computer and then at
each other.  
“Well, you know, it’s just Everything... Everything...” offered Phouchg weakly.  
“Exactly!” said Deep Thought. “So once you do know what the question actually is,
you’ll know what the answer means.”  



## Research Project information 

* This is a group project. It works in a similar way to the mini-projects, but has a longer scope and demands.
* You are expected to use the TCAT (see below) as your main anchor for your research project. This will provide you with a considerable amount of data within a relatively constrained topic. 
* You should also use other tools and sources seen during the course, and you are free to use your own collected data to compare, contrast or complement your research project.
* You don’t need to limit yourselves to digital or quantitative data. The use of non-digital sources (e.g. interviews), and qualitative close reading (e.g. focusing on a sample of comments) is highly encouraged.
* The general topic for the database is *climate change*. You have plenty of freedom on what aspect you want to choose to focus in. Remember that you can look at actors, language use, perceptions, images, networks, news and media, discursive topics, issues and controversies, etc.
* You will have the rest of the term to work on your project
* Look at the 5 questions that **must** be addressed in your final project.

## Basic 5 

#### 1. WHAT (and/or WHO)?
This is your research question(s). What do we *want* to know?, what *can* we know?
* What is the general topic that you'll be addressing? Does it involve an industry? A platform? A social issue? What is the context? A Too vague question may not be answerable. Too specific a question might not be answerable with your data. Try first to define what are that you're interested in. Then narrow a question from ther (This question will keep evolving during your research project).
* Which are the actors involved in your research? Who is made visible? Which actors are "powerful" or "empowered" by the processes you are following? In which specific terms are your defining power? Or how does a notion of power is expressed within your research?
Regarding your man topic, who benefits from a) your methodology, or b) the ideologies related to your topic? Who designed the tools you are using? Who has access to the data you are working with? What kind of information or other phenomena counts as data in this context? 

#### 2. WHY?
Why is the question relevant? Has no one asked this? If so, why is this question still relevant, why are previous answers not satisfactory? What does the answer could change?

**Recommended search engines for literature (in no specific order):**
* Web of Science/Knowledge https://apps.webofknowledge.com/
* Google Scholar: https://scholar.google.com/
* Science Direct https://www.sciencedirect.com/
* Frontiers https://www.frontiersin.org/

#### 3. HOW?
Which kind of data/information do you require? How is this formulated/answerable by the data? Here note down the steps you'll follow to answer the question methodologically (e.g. how did you come up with a website to scrape? How did you selected the source of data? Which recipes did you use to cook your data?)

#### 4. WHY THE HOW?
Why did you choose your specific methodology? What is the logic behind the method? How does the method relates to the question, issues, and actors involved in you research?

#### 5. WHAT NOT?
What do you think is not answerable with your design? Are some parts of your question unanswerable? What is an obvious limitation of your design? What is "out" of data? What is missing from our overview? It missing because it was not included in our sources, because it can be easily adapted to our formats, because it was not evident, or because it is impossible to capture?

## TCAT database
* API based
* Built by keywords *OR* geolocations
* Can be thoroughly queried (if you have a complicated query on a big bin, it will take some time)
* Different data and formats can be exported from it (depending on the amount of data and the bin size, this process may take some time)

### Access: (*unlocked at session 7 or 50,000 wikipoints*)  
Server: https://ds.cc.au.dk/    
Username: tcat   
Password:    

### TCAT BINS

* **Names bin (Ds19cc_nomos)**: climatechange, climatecrisis, climateemergency

* **Aarhus bin (DS19cc_Aarhus)**: klimastrejke Aarhus,klimastrejke Århus,klimaforældrene Århus,klimaforældrene Aarhus,climatechange Aarhus,climatecrisis Aarhus,climateemergency Aarhus,fridaysforfuture Aarhus,actnow Aarhus,actionclimate Aarhus,exctinctionrebelion Aarhus

* **Denmark bin (DS19cc_DK)**: klimastrejke Denmark,klimastrejke Danmark,klimaforældrene Danmark,klimaforældrene Denmark,climatechange denmark,climatecrisis Denmark,climateemergency Denmark,fridaysforfuture Denmark,actnow Denmark,actionclimate Denmark,exctinctionrebelion Denmark

* **Skeptic bin (DS19cc_Skeptic)**: climatechange, globalwarming, climateskeptic, climatedenier

* **Aarhus geolocated bin (DS19_cc_GEO_Aarhus)**
 



