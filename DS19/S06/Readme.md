# Session 6
Proyect Session

**SLIDES**: ([live](http://pablov.me/pres/ds19s06.html) / pdf)

## Plan for the day:
* Data contexts: Redistributing and Reassembling, digitally
* TCAT server
* Research Project

## Data contexts: Redistributing and Reassembling, digitally

**Web social science research (Fielding 2008)**
* digital technologies enable new practices for recording, analyzing, and visualizing social life

**Reassembling (Ruppert et al 2013)**
* Not what digital devices *reveal*, but how they produce and perform the social
* Devices as both: 
 * the *materials* of social life: liveliness of data
 * the *apparatuses* to know it: material, institutional, and behavioural elements

### Digital Methods (big umbrella) 
* Digital methods: “the use of online and digital technologies to collect and anlyse research data (Snee et al 2016)
* Snee et al include:
   * web-based surveys (Dillman 2007)
   * online interviewing and focus groups (Kazmer and Xie 2008)
  * computer mediated discourse-analysis (Herring 2004)
   * digital ethnographies e.g. virtual ethnography (HIne 2000)

**Redistribution (Marres 2012)**
* redistribution of research: not so much an opposition between IT firms and researchers (Savage and Burrows 2007), but a reconfiguration of agents in social research

  **5 views** (along a spectrum):

  * **methods as usual**: old social methodologies incorporated into digital devices)
  * **big-methods**: vast datasets allow us to perform large-scale analysis on *real* network dynamics** 
  * **virtual methods**: adaptation of the social research methods into the digital
  * **digital methods**: adapt digital devices for the purposes of social research

### Digital Methods (Amsterdam School) “debate” (Marres 2017)
 * "Natively digital" methods
> The rise of the Internet enables new research methods that deploy specifically digital devices such as links, comments and shares (Rogers 2009; Rogers 2013)

* The digitalisation of methods
> A focus on the newness of the Internet tends to obscure the extent to which methodological innovation has been a recurrent feature of the social sciences for at least a century (Fielding, 2008)

* The re-mediation of methods
> Digitization enables the re-fashioning of existing social and cultural research methods (Bolter and Grusin 2000; Marres 2012)

#### 1. Native
* methods embedded in online devices: of crawling, scrapping, folksonomy
* digital objects: tweet, username, timestamp, hyperlink
* built upon existing services
* (always a social/cultural enquiry)
###### Spheres (Rogers 2013)
a) mid to late 90s: hyperlink, individual website analysis
b) early to mid 00s: blogosphere, search engine critique
c) late 00s: location-aware, web 2.0, social media

**cross-spherical analysis”: web, blogs, news, twitter, etc**

#### 2. Digitalisation
Newness of methods may lead to distancing users from the aspects of a methodological approach        
Also, a tendency to adapt offline practice to online contents
> Any assesment of Internet methodologies needs to be sober enough to undermine exaggerated claims, but open-minded enough to spot potentiality where it exists (Fielding 2008, 6)

#### 3. Re-mediation
New visual media achieve their cultural significance by paying homage / rivaling / refashioning earlier media
> Remediation did not begin with the introduction of digital media. We can identify the same process throughout the last several hundred years of Western visual representation. A painting by the seventeenth-century artist Pieter Saenredam, a photograph by Edward Weston, and a computer system for virtual reality are different in many important ways, but they are all attempts to achieve immediacy by ignoring or denying the presence of the medium and the act of mediation (Bolter and Grusin 2000, 11)

## Research project +  TCAT
Information for the research project is [located here](research_project.md)

### Activity for the class:
1. Based on the information you have so far on the research project, brainstorm your (guiding) “What” question(s) with your group (you can leave the classroom). Feel free to ask the instructors for guidance.

2. After returning to class, post your question(s) in this pad: https://pad.riseup.net/p/DS19project-session   

## Required Readings
* Ruppert, Evelyn, John Law, and Mike Savage. “Reassembling Social Science Methods: The Challenge of Digital Devices.” Theory, Culture & Society 30, no. 4 (2013): 22–46. (24 p.)
* Marres, Noortje. “The Redistribution of Methods: On Intervention in Digital Social Research, Broadly Conceived.” The Sociological Review 60 (2012): 139–65. (26 p.)

## Recommended readings
* Bolter, Jay David, and Richard Grusin. Remediation: Understanding New Media. Reprint edition. Cambridge, Mass.: The MIT Press, 2000.
* Dillman, Don A. Mail and Internet Surveys: The Tailored Design Method -- 2007 Update with New Internet, Visual, and Mixed-Mode Guide. John Wiley & Sons, 2011.
* Fielding, Nigel G. “The Internet as Research Medium.” In The SAGE Handbook of Online Research Methods, by Nigel G. Fielding, Raymond M. Lee, and Grant Blank. SAGE, 2008.
* Herring, Susan C. “Computer-Mediated Discourse Analysis.” In Designing for Virtual Communities in the Service of Learning, edited by Sasha Barab, Rob Kling, and James H. Gray, 338–76. Cambridge: Cambridge University Press, 2004. 
* Hine, Christine. Virtual Ethnography. First edition. London ; Thousand Oaks, Calif: SAGE Publications Ltd, 2000.
* Kazmer, Michelle M., and Bo Xie. “Qualitative Interviewing in Internet Studies: Playing with the Media, Playing with the Method.” Information, Communication & Society 11, no. 2 (March 2008): 257–78.
* Page, Lawrence, Sergey Brin, Rajeev Motwani, and Terry Winograd. “The PageRank Citation Ranking: Bringing Order to the Web.” Techreport, November 11, 1999. http://ilpubs.stanford.edu:8090/422/.
* Rogers, Richard. The End of the Virtual: Digital Methods. Amsterdam University Press, 2009.
* Rogers, Richard. Digital Methods. MIT Press, 2013.
* Rogers, Richard. “Digital Methods for Web Research.” In Emerging Trends in the Social and Behavioral Sciences, edited by Robert A Scott and Stephan M Kosslyn, 1–22. Hoboken, NJ, USA: John Wiley & Sons, Inc., 2015.
* Savage, Mike, and Roger Burrows. “The Coming Crisis of Empirical Sociology.” Sociology 41, no. 5 (October 2007): 885–99.
* Snee, Helene, Christine Hine, Yvette Morey, Steven Roberts, and Hayley Watson. Digital Methods for Social Science: An Interdisciplinary Guide to Research Innovation, 2016. http://www.dawsonera.com/depp/reader/protected/external/AbstractView/S9781137453662.
