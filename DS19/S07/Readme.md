# Session 7
Block 3: Methods and Networks

**SLIDES**: ([live](http://pablov.me/pres/ds19s07.html) / pdf)

## Downloads & installations prior to session:
* Install gephi https://gephi.org/ 
* Create an account in issuecrawler (1 person per group is needed, more is optional) https://issuecrawler.net/index.php?requestaction=changepage&changepagename=Apply  

## Plan for the day:
* Data contexts: Network Analysis
* Tool: Gephi

## Data contexts: Network Analysis

### Graph theory

The seven bridges of Königsberg problem: is it possible to cross each bridge once and return to the starting point?

<img src="koenigsberg.png" width="500"><br>   
Königsberg  

Leonhardt Euler solved the problem, and inaugurated the field of graph theory: all points must have an even degree (an even number of connections).


<img src="euler-graph.jpg" width="500"><br>   
Euler Graph  

* Baran (1964)	
  * Structures based on Nodes and Edges 
  * Statistical/Quantitative relations: clusters, centrality, density (see graph glossary below)
  * Centralized, Decentralized and Distributed

<img src="baran.png" width="500"><br>   
Baran (1964)

### Sociometrics
* Moreno (1934)
  * Discover social life through network properties & topologies
  * Analysis of relational data
  * Gain insight into social relations and make them available to intervention (Guggenheim 2012) -> participants become observers of their own problems

<img src="moreno-rooms-girls.png" width="500">  
<img src="moreno-topological-girls.png" width="500"><br>    
Likes and dislikes in a girl's boarding school (Moreno 1934 in Gießmann 2017)

### Visual Network Analysis (see Glossary below)

* evolving, non-deterministic data
* force-vector: *gravity*
* not spatial, but relative position of the nodes (in relation to each other)
* borders are not exact, communities are fluid and can overlap
* mixed methods: qualitative inquiry before, after, or alongside the quantitative analysis

**Actors in a social network:**
* sources
* leaders
* commenters
* lurkers
* bridges
* hubs
* etc.

<img src="venturini-clusters.png" width="500"><br>   
Clusters<br>
<img src="venturini-stars.png" width="500"><br>   
Stars<br>
<img src="venturini-cliques.png" width="500"><br>   
Cliques<br>
<img src="venturini-authorities-hubs.png" width="500"><br>   
Authorities and Hubs<br>
<img src="venturini-table.png" width="500"><br>   
Qualitative work behind the network<br>
<img src="venturini-languages-categories.png" width="500"><br>   
Different categories in the same network<br>

#### Glossary
* **node**: point in a graph   
* **edge**: connection between two points in a graph  
* **directed edge**: it has a direction (e.g. *x* replies to *y*)  
* **undirected edge**: it does not have  a direction (e.g. two co-appearing hashtags in a sentence)  
* **cluster**: a group with relatively small distances among the nodes (they can be intermediary [in the middle of other clusters] or peripheral [far from the center])  
* **bridges**: nodes or clusters that have connections with several other clusters
* **subcluster**: clusters within a bigger cluster  
* **in-degree**: quantity of incoming connections to a node  
* **out-degree**: quantity of outgoing connections from a node  
* **size**: number of nodes (in a network or cluster)
* **centrality**: the most important nodes in the network (according to “degree”), it can be global (the whole network), or local (a cluster)
* **authorities**: nodes with a high number of in-degree edges 
* **hubs**: nodes with a high number of out-degree edges    
* **density**: number of connections divided by the potential number of connections (a highly dense network has a lot of edges, regardless of the number of nodes)  
* **main component**:  
* **structural holes**: empty zones in between clusters, denote absence of connections   
* **star**: centralized structures, denote an authority or a hub  
* **cliques**: group of nodes with many connections between each other  
* **typology**: category of a node, edge, or cluster  
* **topology**: spatial localization of a node, edge, or cluster  

## Tools
### Gephi
https://gephi.org  
* Open Source  visualisation and exploration of graphs (networks)
* Based on java
* Works with .gexf and .gdf native files, or with 2 tabular files (a node spreadsheet and an edge spreadsheet)

#### DATA LABORATORY TAB
* Here you can see 2 tables:  
a. A node table (which includes a node ID, along other columns)  
b. An edges table (which includes an edge ID, source node ID, and target node ID, along other columns)
* You can modify the content directly on the tables, or add more tables manually. However, this is not recommended at this moment. 

#### OVERVIEW TAB
* You have 3 windows: 
  * **Appearance**: allows you to change the size, color, and labels of the nodes and edges of your graph,  according to certain rules
  * **Layout**: allows you to change and tweak the layout of the graph (the "gravity" rules)
  * **Graph**: here you have a visual of the graph, an some direct access to changes in the appearance. Useful buttons here are "Center on Graph" (low left), which allows you to recenter the graph if you get lost; and "Edit, edit node attributes" (middle left), which allows you to see and modify a specific node information without going to the data laboratory tab.
  * **Context**: shows basic information of the graph (number of nodes, edges, and if it is directed)
  * **Filters and Statistics** : will show you statistics and allow you to filter parts of the information within the graph (we won't use this window for the moment)

#### PREVIEW TAB

- The preview tab will show a stylised version of the graph (you'll have to click refresh to see new changes), mostly to export a final visual. You can modify what is shown, and export as a a vector or bitmap image, or pdf document (svg, png, pdf). 



**Gephi quickstart guide**: https://www.slideshare.net/gephi/gephi-quick-start 

### Activity (max. 2 people)
**Example Data**:
1. Download the any of the example files: 

* A. [Ds19cc_nomos-20190919-20190921-greta-----------interactionGraph-min4nodes-1dfc4bf59a.gexf](Ds19cc_nomos-20190919-20190921-greta-----------interactionGraph-min4nodes-1dfc4bf59a.gexf)  
Type: Social graph by in_reply_to_status_id (directed, minimum 4 replies)  
Bin: nomos  
Query: greta  
Dates: 2019-09-19 / 2019-09-21  

* B. [DS19cc_Skeptic-20190919-20190921-greta-----------mention-_Top1000-1dfc4bf59a.gdf](DS19cc_Skeptic-20190919-20190921-greta-----------mention-_Top1000-1dfc4bf59a.gdf)  
Type: Social graph by mentions top 1000 mentions (directed)  
Bin: skeptic  
Query: greta  
Dates: 2019-09-19 / 2019-09-21  

* C. [DS19cc_Skeptic-20190919-20190921-greta-----------urlHashtag--1dfc4bf59a.gexf](DS19cc_Skeptic-20190919-20190921-greta-----------urlHashtag--1dfc4bf59a.gexf)   
Type: Bipartite hashtag-URL graph (undirected)  
Bin: skeptic  
Query: greta  
Dates: 2019-09-19 / 2019-09-21   

2. Open gephi (it should be an executable).   
3. Select one of the sample files.  
4. Open graph -> check information -> directed. A new graph should appear (nodes appear randomly)  
5. **Make sure to note down all your steps, remember there is no undo button** 🤷‍  

**LAYOUT WINDOW**  
6. Change the layout: select “choose a layout” in the left menu and press “run”  
* “Force Atlas 2” will *exaggerate* the distance of the nodes (you’ll see more clearly which nodes are closer to each other)
* “Fruchterman Reingold” will try to position the nodes in a spherical model
* “Label Adjust” will create some space between nodes for label readability
* “Noverlap” will create distance in overlapping nodes (so you can see all the nodes at once)
* Each time you run a new layout it will run on the previous layout (is an additive process). This means that you need to experiment with the layouts first, to know which one will be useful.
7. Identify structures from the glossary.   

**APPEARANCE WINDOW**  
8. Change the size of the nodes, according to their in-degree (remembert to press "Apply" to see your changes)  
9. Change the color of the nodes, according to their out-degree    

**GRAPH WINDOW**  
10. Open the bottom menu  
11. Make the labels visible (tab labels -> click on node square), and change their size to "node size"  
12. (if your labels are not very informative, press "configure" and select a more appropiate node attribute to show as label)  
13. Go to the Preview tab and export your graph  

### Activity (groups)
1. You now have access to the TCAT server (see the [project page](research_project.md)  for the URL/user/password)
2. Continue working on your research question
3. Try some queries to familiarize with the kind of data you have access to now (remember that high activity on the server, as well as long queries will take time to process. It is better to start with small queries or small bins). You get statistics, data, or networks files.
4. Think about what bin or bins work better to help advance your research question

## Mini project B3: 
The goal for this project is less focused on the general rationale or description of methodological steps, and more on defining the rational basis for the chosen methods.

For the third mini-project you should use at least one of the tools from this block (you can also use tools from previous blocks): *Carto* and/or *Gephi*. Try to use (if applicable) the topics of this and previous blocks (for this mini-project the questions on ethics are particularly relevant).
The main question that should be addressed by your **~~second~~ third** mini-project is:

***Why the How?***

Why did you choose your specific methodology? What is the logic behind the method? How does the method relates to the question, issues, and actors involved in you research?

As usual, this is an exploratory mini-project, so don't worry about finding the *right* answers, or even asking the perfect question. However, pay special attention to justifying your use of the method. 

As before, the mini-project **will be presented next week during class. Groups presenting will be randomly chosen on Tuesday before noon.** Some groups will also be chosen as replyers to comment on the work of your peers. However, **all groups should populate their wiki-projects.**

## Required Readings
* Venturini, Tommaso, Liliana Bounegru, Mathieu Jacomy, and Jonathan Gray. 2017. ‘How to tell stories with networks’. In The Datafied Society. Studying Culture through Data, 155–69. Amsterdam University Press. (14 p.)
* Bruns, Axel, and Jean Burgess. “The Use of Twitter Hashtags in the Formation of Ad Hoc Publics,” 2011, 9. (9 p.)
* Page, L., Brin, S., Motwani, R., & Winograd, T. (1999). The PageRank citation ranking: Bringing order to the web. Stanford InfoLab. (17 p.) [ignore equations!]

## Recommended readings
### NETWORKS
* Baran, P. 1964. ‘On Distributed Communications Networks’. IEEE Transactions on Communications Systems 12 (1): 1–9.
* Gießmann, Sebastian. 2017. ‘Drawing the Social: Jacob Levy Moreno, Sociometry, and the Rise of Network Diagrammatics’. Collaborative Research Center 1187 Media of Cooperation, Working Paper Series, , December. 
* Guggenheim, Michael. “Laboratizing and De-Laboratizing the World: Changing Sociological Concepts for Places of Knowledge Production.” History of the Human Sciences 25, no. 1 (February 2012): 99–118.
* Heidler, R., Gamper, M., Herz, A., & Eßer, F. (2014). Relationship patterns in the 19th century: The friendship network in a German boys’ school class from 1880 to 1881 revisited. Social Networks, 37, 1–13.
* Latour, Bruno. 2007. Reassembling the Social: An Introduction to Actor-Network-Theory. OUP Oxford.
* Venturini, Tommaso, Mathieu Jacomy, and Pereira D Carvalho. 2015. ‘Visual Network Analysis’. Sciences Pomedialab.
* Mayer, Katja. “Objectifying Social Structures: Network Visualization as Means of Social Optimization.” Theory & Psychology 22, no. 2 (April 2012): 162–78. 
* Sánchez, Sandra Álvaro. “A Topological Space For Design, Participation And Production. Tracking Spaces Of Transformation,” no. 13 (n.d.): 15.
### TCAT
* Borra, Erik, and Bernhard Rieder. “Programmed Method: Developing a Toolset for Capturing and Analyzing Tweets.” Edited by Dr Axel Bruns and Dr Katrin Weller. Aslib Journal of Information Management 66, no. 3 (May 19, 2014): 262–78.


