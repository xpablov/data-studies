# Session 8
Block 3: Methods and Networks

**SLIDES**: ([live](http://pablov.me/pres/ds19s08.html) / pdf)

## Plan for the day:
* Presentations
* Data contexts: ANT & Issue Mapping
* Mid-term survey results

## Data contexts: ANT & Issue Mapping

### Actor Network Theory (Latour 2007; Latour et al 2012)
Actor Network Theory emphasizes a "flat" approach the social, and argues that it can be study as a network consisting of any kind of elements.

* Relations between “technology” and “society”
* Heterogeneous networks
* 1 level: neither nature and society, nor micro and macro 
* Actors: computers, text, humans, etc (and mostly the places they "colide")
* Properties emerge from networks
* No point of departure (but an epistemological point of entry)

> More subtle than the notion of system, more historical than the notion of structure, more empirical than the notion of a complexity, the idea of network is the Ariadne's thread of these interwoven stories (Latour 1993)

### Issue Mapping

* Network clusters do not necessarily show communities
* Not an info-network
* Not a social-network
* Not (only) a hot-topic
* Beyond medium-specificity

> Issue-network “a heterogeneous set of entities (actors, documents, slogans, imagery) that have been configured into a hyperlink-network around a common problematic” (Marres and Rogers 2005)
> 
<img src="wcit.png" width="500">  

Profiling hashtags during the World Conference on International Telecommunications summit in 2012<br>
([see the project information here](http://blogs.cim.warwick.ac.uk/issuemapping/cases/wcit-profiles/))

<img src="tube_topfrequency_no_lines_copy.jpg" width="500">

Top hashtags in the Climate Change space on Twitter over time (March-Jun 2012)<br>
([see project information here](http://blogs.cim.warwick.ac.uk/issuemapping/cases/issue-lifelines/))

### Issuecrawler: 
https://issuecrawler.net

**Crawling methods:**  
a. co-link network: network only retains and crawls "shared" links between 2 or more sites for each iteration  
b. snowball: network crawls all links   
c. inter-actors: network crawls only links directly in-between the sites  

**Example:**
Snowball methods
Starting points: 
* http://noah.dk/english
* https://dk.usembassy.gov  
FILE: [ic_network_351466.core.gexf](ic_network_351466.core.gexf) (can be opened in gephi)

<img src="noahDK.png" width="500">

## Required Readings
* Latour, Bruno, Pablo Jensen, Tommaso Venturini, Sébastian Grauwin, and Dominique Boullier. 2012. ‘“The Whole Is Always Smaller than Its Parts”–a Digital Test of Gabriel Tardes’ Monads’. The British Journal of Sociology 63 (4): 590–615. (25 p.)
* Marres, Noortje. 2015. ‘Why Map Issues? On Controversy Analysis as a Digital Method’. Science, Technology & Human Values, 0162243915574602. (24 p.)

## Recommended readings
* Bucher, Taina, and Anne Helmond. “The Affordances of Social Media Platforms.” In The SAGE Handbook of Social Media, by Jean Burgess, Alice Marwick, and Thomas Poell, 233–53. 1 Oliver’s Yard, 55 City Road London EC1Y 1SP: SAGE Publications Ltd, 2018.
* Gibson, Rachel, Marta Cantijoch, and Stephen Ward. Analysing Social Media Data and Web Networks, 2014.
* Latour, Bruno. We Have Never Been Modern. Cambridge, Mass: Harvard University Press, 1993.
* Latour, Bruno. Reassembling the Social: An Introduction to Actor-Network-Theory. OUP Oxford, 2007.
* Lury, Celia, and Nina Wakeford. “Introduction: A Perpetual Inventory.” In Inventive Methods: The Happening of the Social, 1–25. Routledge, 2012.
* Marres, Noortje. “Net-Work Is Format Work: Issue Networks and the Sites of Civil Society Politics.” In Reformatting Politics, 33–48. Routledge, 2013.
* Marres, Noortje, and Richard Rogers. “Recipe for Tracing the Fate of Issues and Their Publics on the Web,” 2005.
* Marres, Noortje, and Esther Weltevrede. 2013. ‘Scraping the Social? Issues in Live Social Research’. Journal of Cultural Economy 6 (3): 313–335. (21 p.)
* Mayer, Katja. “Objectifying Social Structures: Network Visualization as Means of Social Optimization.” Theory & Psychology 22, no. 2 (April 2012): 162–78. 
* Rogers, Richard. Doing Digital Methods. Thousand Oaks, CA: SAGE Publications Ltd, 2019.





