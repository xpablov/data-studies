# Session 1

BLOCK 0: Ethos

**SLIDES**: ([live](http://pablov.me/pres/ds19s01.html ) / pdf)

## Intro

### Plan for the day:
* course structure
* Introductions
* survey *results*
* activities: pads + markdown (easy + fun!)

### Learning outcomes
* **Knowledge**:
    * Demonstrate an understanding of the role of data in society
    * Critically reflect on the use of data to conclude general conditions in the world and in digital environments
* **Skills**:
    * Use digital tools to collect, analyse and present data
    * Critically reflect on the production and use of data in specific cases
* **Competences**:
    * Critically analyse and consider the role of data in society, as well as the use and design of digital technologies for data collection and production

##  Course structure
* 14 sessions in 5 blocks
* lectures + tools intro (group troubleshooting)
* workshops (discussion + troubleshoot)
* groupwork mini projects
* presentations (2n week each block)
* wiki-project
* wiki-thoughts
* peer-reviews (rubric)
* final project (intro in week 41)
* final presentation (W49)
* exam (W02 2020)

<img src="wiki-p.png" alt="drawing" width="500">

## GDPR & SURVEY
## Survey intro
* Introductory issues:
    * incomplete sample
    * irregular inputs 

### GDPR + ETHICS
* GDPR (General Data Protection Regulation) https://ec.europa.eu/commission/priorities/justice-and-fundamental-rights/data-protection/2018-reform-eu-data-protection-rules_en
* Active as of May 2018
* Applies to all companies based in EU
* Applies to all *processors* EU citizen’s data

**Important Keeywords:**

*  *Processing* (articles 4 and 6): “collection, recording, organisation, structuring, storage, adaptation or alteration, retrieval, consultation, use, disclosure by transmission, dissemination or otherwise making available, alignment or combination, restriction, erasure or destruction“

* the right to be forgotten 
    * Data no longer being relevant to original purposes for processing, or a data subject withdrawing consent (Article 17)

* portability: ‘commonly use and machine readable format’ back to the user

* Personal data (mainly articles 2, 4, 5): identifiable or re-identifiable
    * a name and surname;
    * a home address;
    * an email address such as name.surname@company.com;
    * an identification card number;
    * location data (for example the location data function on a mobile phone);
    * an Internet Protocol (IP) address;
    * the advertising identifier of your phone;
    * data held by a hospital or doctor, which could be a symbol that uniquely identifies a person.

* Sensitive data
    * ethic origin
    * political, religious, and philosophical beliefs
    * trade union affiliations
    * genetic / biometric data
    * health
    * sex life & sexual orientation  

* Terms of agreement
    * due to GDPR, request for consent must be given in an intelligible and easily accessible form

<img src="tyson_data.jpg" alt="drawing" width="300">

**Questions when designing our research / working with data:**

* Is this ethical?
* Can this harm groups or individuals? Whom?
* Does this violates regulation (is this legal)? Made by whom?
* Is this legitimate?
* Who uses twitter/reddit, etc?
* Whenever we use a tool: who made this? Is my data safe? How can I know that? Is it open/close? Free for some * populations? Which company is behind it?

## Tools

### Pads
https://pad.riseup.net/p/DS19testbed
(more info at: https://etherpad.org)

### Gitlab/Markdown
* Markdown
Syntax guide: https://www.markdownguide.org/basic-syntax  
Recommended editor (optional): https://typora.io/

## Activities
### INSTRUCTIONS:
Groups: 2-3 people next to you
1. Start a sharepad
2. Select a question to answer (A, B, or C)
3. Use markdown for your writing

A) Read the TOA of a platform/ institution. Is it legible? How long does it take you to extract relevant information. Is it cryptic? Think how intuitions differ from individuals (does Twitter behaves in the same way as it expects it's users to behave?) (*1200 wikipoints*)

B) Estimate how many platforms/services have you signed up to (e.g. everytime you type your email in a form). Is this even possible? How to get a better idea? (E.g. see which apps are associated to you facebook/facebook/twitter accounts) (*500 wikipoints*)

C) How would you re-design this survey in order to consider diversity of data, and make it more ethical? (*8000 wikpoints*)

When finished, paste the final text and the link in todays Gitlab Wiki [B0-WR](https://gitlab.com/xpablov/data-studies/wikis/B0-WR )
What? You haven't signed up for Gitlab? Do it now, and request access to the gitlab course

### Pads
https://pad.riseup.net/
(more info at: https://etherpad.org)
#### Markdown
Syntax guide: https://www.markdownguide.org/basic-syntax
Recommended editor (optional): https://typora.io/


## Required readings
* Rogers, Richard. “Foundations of Digital Methods.” In The Datafied Society. Studying Culture through Data, 75–94. Amsterdam University Press, 2017. (20 p.) 
* Annette N. Markham. “Research Ethics in Context.” In The Datafied Society. Studying Culture through Data, edited by van Es Karin and Schäfer Mirko Tobias, 201–9. Amsterdam University Press, 2017. (19 p.)

## Recommended readings
* Hewson, Claire. “Ethics Issues in Digital Methods Research.” In Digital Methods for Social Science: An Interdisciplinary Guide to Research Innovation, edited by Helene Snee, Christine Hine, Yvette Morey, Steven Roberts, and Hayley Watson, 2016. http://www.dawsonera.com/depp/reader/protected/external/AbstractView/S9781137453662. (16 p.)
* Markham, Annette. “Ethic as Method, Method as Ethic: A Case for Reflexivity in Qualitative ICT Research.” Journal of Information Ethics 15, no. 2 (November 1, 2006): 37–54. https://doi.org/10.3172/JIE.15.2.37. (17 p.)
Kaiser, Jonas, and Adrian Rauchfleisch. “The Implications of Venturing down the Rabbit Hole.” Internet Policy Review, June 27, 2019. https://policyreview.info/articles/news/implications-venturing-down-rabbit-hole/1406. (8 p.)
* Kotsios, Andreas, Matteo Magnani, Luca Rossi, Irina Shklovski, and Davide Vega. “An Analysis of the Consequences of the General Data Protection Regulation (GDPR) on Social Network Research.” ArXiv:1903.03196 [Cs], March 7, 2019. http://arxiv.org/abs/1903.03196.

