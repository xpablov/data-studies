# Session 10

Block 4: Big Data, Big Politics

**SLIDES**: ([live](http://pablov.me/pres/ds19s10.html) / pdf)

## Plan for the day:

* Presentations
* Data contexts: Dataveillance
* Jupyter notebooks + Pandas dataframes

### Data contexts: Dataveillance

<img src="bentham.png" width="500">  

Jeremy Bentham (*actually* Jeremy Bentham)

<img src="panopticon.png" width="500">  

Panopticon-based prision

#### Digital Panopticon

* Bentham's panopticon
* Disciplinary society (Foucault 1975)
* Sur-veillance: watching-over

* Panoptic surveillance (Elmer 2003)
  * few observing many ("unequal gaze")
  * internalized regulatory gaze
* Panspectric veillance (De Landa 1991)
  * enabled by sensors and signals, instead of human senses
* Digital Surveillance: 
  * Bigger scope of data: CCTV, RFID, biometics... 

#### Short activity: 

In pairs: 

1. Make a list of objects/devices (*physical* or digital) that have tracked you this week
2. Which of them are obviously visible? Which are obfuscated?
3. Who is gathering this information? Do you know to what purpose?


#### Capture all

* "Liquid surveillance" (Lyon & Bauman 2013)
  * fluid, uncontained, pervasive monitoring 
* van Dijck (2014)
  * *Dataification*: 
    * social actions turned into quantified data (e.g. Moll's Dating Brokers project  https://datadating.tacticaltech.org/viz)  
  * *Dataveillance*:
    * continous tracking of (meta)data for **unstated** purposes
  * *Dataism*
    *  ideology of neutrality

> Dataism thrives on the assumption that gathering data happens outside any preset framework (...) and data analysis happens without a preset purpose

#### Social inter-veillance

* Social  surveillance:
  * self-tracking (Lupton 2014) 
  * *participatory* surveillance (Marwick 2012): watching each other, being viral)

* Behavioural knowledge, manipulation, and prediction: microtargeting as a combination of politics and consumer marketing, and the capacity of manipulation of affective states (O'Neil 2017)

* Predictive policing (e.g. Persistent Surveillance Systems)

<img src="radiolab-surveillance.png" width="500">  

Persistent Surveillance Systems

**Current moment:**

state +  corporations + (big) data + platforms + economy model + value

> With political messaging, as with most WMDs (Weapons of Math Destruction), the heart of the problem is almost always the objective. Change that objective from leching off people to helping them, and a WMD is disarmed -and can even become a force for social good (O'Neill 2017)



## Jupyter notebooks + Pandas dataframe

### Instructions:

1. Download the .ipynb file to an easy to access folder
2. Download the csv example file to the same folder
3. Open Anaconda Navigator
4. Launch Jupyter notebook
5. You will see a file structure from your computer
6. Navigate to your .ipynb  file to open it
7. After exploring the cells activation, try to open and modify another file of your choice

**Notebook**: [DS19 Introductory data handling with Jupyter notebooks (.ipynb file)](DS19-intro.ipynb) (*right click -> save as...*)  
**csv example file**: [tcat_Ds19cc_nomos-20190917-20190921-greta-----------randomTweets-1000-1dfc4bf59a.csv](tcat_Ds19cc_nomos-20190917-20190921-greta-----------randomTweets-1000-1dfc4bf59a.csv) (*right click -> save as...*)  

## Required Readings

* Dijck, Jose van. 2014. ‘Datafication, Dataism and Dataveillance: Big Data between Scientific Paradigm and Ideology’. Surveillance & Society 12 (2): 197–208. (11 p.) 
* Cathy, O’Neil. Weapons of Math Destruction: How Big Data Increases Inequality and Threatens Democracy. 1 edition. London: Penguin, 2017. (Chapter 10, 18 p.)

## Recommended readings

* David Lyon, and Bauman, Zygmunt. Liquid Surveillance: A Conversation. 1 edition. Cambridge, UK ; Malden, MA: Polity, 2012.
* Elmer, Greg. “A Diagram of Panoptic Surveillance.” New Media & Society 5, no. 2 (June 1, 2003): 231–47.
* Foucault, Michel. Discipline and Punish: The Birth of the Prison. Vintage. Knopf Doubleday Publishing Group, 2012 [1975].
* Gitelman, Lisa. Raw Data Is an Oxymoron. MIT Press, 2013. (Chapter 7: Dataveillance and Counterveillance, 24 p.)
* Landa, Manuel De. War in the Age of Intelligent Machines. New York, NY: Zone Books, 1991.
* Lupton, Deborah. The Quantified Self. John Wiley & Sons, 2014.
* Mann, S. (2016). Surveillance (Oversight), Sousveillance (Undersight), and Metaveillance (Seeing Sight Itself). 2016 IEEE Conference on Computer Vision and Pattern Recognition Workshops (CVPRW), 1408–1417.
* Marwick, Alice E. “Social Surveillance in Everyday Life.” Suveillance & Society 9, no. 4 (2012): 378–93.
* Srnicek, Nick. 2016. Platform Capitalism. 1 edition. Cambridge, UK ; Malden, MA: Polity.

## Other Resources

* Podcast: Radiolab. “Eye in the Sky | Radiolab.” Accessed July 13, 2019. https://www.wnycstudios.org/podcasts/radiolab/articles/eye-sky.
* Podcast: Note to Self. “When Your Conspiracy Theory Is True.” Accessed November 13, 2019. https://www.wnycstudios.org/podcasts/notetoself/episodes/stingray-conspiracy-theory-daniel-rigmaiden-radiolab.
* Moll, Joanna and Tactical Tech, The Dating Brokers. https://datadating.tacticaltech.org/viz. Accessed August 15, 2019. 