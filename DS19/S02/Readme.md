# Session 2
Block 1: Data Capture  

**SLIDES**: ([live](http://pablov.me/pres/ds19s02.html) / pdf)

## Plan for the day:
* Raw / Cooked data
* Data taxonomies & Machine readable data
* Crawling with *Dataminer*
* Cleaning with *Openrefine*
* Visualising with *Raw*
* Mini-projects set-up

## Data Taxonomies

### "Raw" data: 
* Neither “transparent” nor “self-evident”
* Objectivity is historical
* Data vs fact (Rosenberg)
* Before, during, after

<img src="record.png" alt="drawing" width="500">

### Data != Capta
* Data != capta (Jensen 1950 in Bekker 1952)
* Not passive acceptance, but active construction (Drucker 2011)
* Knowledge as produced, more than discovered (Gitelman 2013)

### Data taxonomies
* **Data *is* (Rosenberg 2013):**
	* Abstract
	* Discrete
	* Aggregative
	* Meaningful
* **Data *is* (Floridi 2010):**
	* Taxonomical
	* Typological
	* Genetic
	* **But also differs on perspectives:**
		* Epistemic
		* Informational
		* Computational

<img src="codex_s.jpeg" alt="drawing" width="500">
Codex Seraphinianus
<img src="voynich.jpg" alt="drawing" width="500">
Voynich manuscript

* **Data *can* be (Kitchin and Tate 1999):**
	* Quantitative     
		* Nominal
		* Ordinal
	* Qualitative
	* Structure
		* Structured
		* Semi-structured
		* Unstructured
	* Source
		* Captured
		* Exhaust
		* Derived
	* Type
		* Indexical
		* Attribute
		* Metadata
* **Data *should* be FAIR (Wilkinson et al 2018)**
	* Findability
	* Accessibility
	* Interoperability
	* Reusability
* **Data *should* be Smart (Schöch 2013):**
	* Structured (or semi-structured)
	* Enriched
	* Small

### Short activity
**What about *good* data?**
Choose one (1-3 people):
* Explore the [*noonies*](https://noonies.hackernoon.com/award/cjxvrv4p26gd40b40cdlfmwwy). These prizes are given to "emerging tech to advance social or environmental progress". Do they involve the use of open/fair data? Is it clear how they could improve the social life of others (beyond stating it)?
* Check the [open data index](https://index.okfn.org/ ) for Data in Denmark, and compare it to other country you are familiar with. How data differs? Is it readable? Open licensed? Accessible? Are the formats machine readable? Public? Free?

## Tools
### Data Scraper/Miner
https://data-miner.io/ 
* Chrome plugin (does not work in other browser)
* More info: https://data-miner.io/user-manuals/custom-scripts

**Instructions:**
1. Start page: https://www.boligportal.dk/en/find?placeIds=72
2. Launch Data Miner -> New Recipe 
3. Choose page type: “List” is used for multiple pages  

ROW TAB  
4. Go to Rows tab -> click "Find" -> hover the information you are trying to scrape (make sure the selection box is as exact as possible)  
5. Press **shift** (DO NOT click)  
6. Select one of the suggested html elements/classes/ids  
7. You should have a green outline wrapping the individual row containers  

COLUMN TAB  
8. Click on the Column tab -> select “column 1”  
9. You can name the column, and select the data type.  
10. Click the "Find" button-> hover -> shift over the data  
11. Pick the class or HTML element that highlights the data the best (use the "Select Parent" button for more options if the selector outlines too many other items)  
12. Once the data is highlighted, click "confirm". You can click the Eyeball to check the data is correct.  
13. Repeat the previous steps for all the columns you want  

NAVIGATION
14. Click on the Nav Tab.  
15. Click the "Find" -> Locate the next page button on the web page.  
16. Hover your mouse over the next page button -> shift  
17. Select one of the suggested selectors and hit confirm.  
18. Click "Test Navigation" to see if it works  
19. (If it doesn't work, try choosing a sibling element. Otherwise, a selector that often works is ‘a:contains(Next)’)  

RUN  
20. Click the Save tab. Here you can name the recipe, click save and then run to scrape.  
21. After it runs, click download to save the data (CSV is recommended)  

### OpenRefine
http://openrefine.org/  

**Guide for common uses**: https://datacarpentry.org/openrefine-socialsci/   
**Open refine tutorial with example data**: http://miriamposner.com/classes/dh101f17/tutorials-guides/data-manipulation/get-started-with-openrefine/  

1. Download and run (installation instructions: https://github.com/OpenRefine/OpenRefine/wiki/Installation-Instructions)
2. Open browser at http://127.0.0.1:3333/ (if not already open)
3. Click browse to add your data -> next
4. Check that your data looks correctly uploaded -> Create project

#### Common cleaning strategies:
**Remove blank spaces:**
* (Click arrow at the beginning of column) -> edit cells -> common transforms -> trim...

**Bulk edit:**
* Press edit button in cell -> change cell value -> Apply to all identical cells

**Transform Data:**
* edit cells -> Transform..
* `value.replace("x", "y")`

**Filtering:**
* column arrow -> text filter

**Sort:**
* sort...

**Change data type:**
* edit cells -> common transforms -> to...
OR
* press edit button in cell and change data type

**Split:**
* Add column based on this column...
* `value.split(" ")[1]`

Facets:
**Remove duplicates:**

* Facet -> Customized Facets

Export
* Export .-> comma-separated value

Advanced users (GREL): https://github.com/OpenRefine/OpenRefine/wiki/General-Refine-Expression-Language 

## Raw
https://app.rawgraphs.io/  
Try your datasets in raw. You can just upload your files directly. 

What kind of data do you have (strings, numbers, dates?). Depending on this, we can use different visualizations with Raw (check their data samples).

Use the different datasets produced and gathered in the class, and try to make a visualisation. Think about which data types do you have: numbers, booleans, dates, text. See which visualsation fits better. 

## Mini project B1: Collect, clean, visualize
For the first mini-project you should use the tools from these session to collect data (*dataminer*), clean it and manipulate it as needed (*openrefine*), and find a suitable visualisation (*raw*). Try to use (if applicable) the taxonomies seen on this session to describe your data.

One main question should be addressed by your first mini-project: 

1. **How?**
* Which kind of data/information do you require? How is this formulated/answerable by the data? Here note down the steps you'll follow to answer the question methodologically (e.g. how did you come up with a website to scrape? How did you selected the source of data? Which recipes did you use to cook your data?). You should also include some thoughts on the ethics of your mini-project.

This is an explorative mini-project, so don't worry about finding the right *answers*. Here is more interesting to show your attempts, what worked, and what didn't. Rememeber we are always being reflexive about applying methods and techniques. 

**Notes on your mini-project procedure should be added to the wiki-project (your thoughts on the questions, and the steps you took during your scrape, clean, visualize mini project)**. This is a group work, so only one entry per group is needed.

The mini-project **will be presented next week during class. Groups presenting will be randomly chosen on Tuesday before noon**. Some groups will also be chosen as replyers to comment on the work of your peers. 
However, **all groups should populate their wiki-projects** (as a group, not as individuals). To do this:
1. Go to the gitlab wiki
2. Click on your workshop animal
3. Go to *X*-B1-WR
4. Create a new a link to a (new) page:  type `[Link Title](page-slug)`.  (e.g. `[Group Alpha](alpha-b1-wr)`)
5. Write your report in your new page.



## Required Readings
* Daly, Angela, Kate Devitt, and Monique Mann, eds. Good Data. Theory on Demand 29. Amsterdam: Institute of Network Cultures, 2019. (Chapter 10: Making Data Public? The Open Data Index as Participatory Device, 15 p.)
* Gitelman, Lisa. Raw Data Is an Oxymoron. MIT Press, 2013. (chapter 1: Data before the fact, 25 p.)

## Recommended readings
DATA TAXONOMIES
* Becker, Howard. Through Values to Social Interpretation; Essays on Social Contexts, Actions, Types, and Prospects. Through Values to Social Interpretation; Essays on Social Contexts, Actions, Types, and Prospects. Oxford, England: Duke Univ. Press, 1950.
* Drucker, Johanna. “Humanities Approaches to Graphical Display.” Digital Humanities Quarterly 005, no. 1 (March 10, 2011).
* Floridi, Luciano. Information: A Very Short Introduction. OUP Oxford, 2010. https://www.statsbiblioteket.dk/au/?locale=en#/search?query=recordID:"ebog_ssj0000523414"
* Kitchin, Rob, Nick Tate, and Nick Tate. Conducting Research in Human Geography : Theory, Methodology and Practice. Routledge, 2013.
* Kitchin, Rob. The Data Revolution: Big Data, Open Data, Data Infrastructures and Their Consequences. SAGE, 2014.
* Schöch, Christof. “Big? Smart? Clean? Messy? Data in the Humanities.” Journal of Digital Humanities, November 22, 2013. http://journalofdigitalhumanities.org/2-3/big-smart-clean-messy-data-in-the-humanities/.
* Wilkinson, Mark D., Michel Dumontier, IJsbrand Jan Aalbersberg, Gabrielle Appleton, Myles Axton, Arie Baak, Niklas Blomberg, et al. “The FAIR Guiding Principles for Scientific Data Management and Stewardship.” Scientific Data 3, no. 1 (December 2016): 160018. https://doi.org/10.1038/sdata.2016.18.
