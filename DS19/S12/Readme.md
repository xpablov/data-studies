# Session 12

Block 4: Data Logic & Resistance Tactics

**SLIDES**: ([live](http://pablov.me/pres/ds19s12.html) / pdf)

## Plan for the day:

* Presentations
* Data contexts: Resistance Tactics 
* Recap

## Data contexts: Resistance Tactics 

### Open / Obfuscated / Tactically visible / Data Activism

  * Surveillance, activism, and good practices
  * Case: start of geolocation: Knobel vs Yahoo vs Houri ( Goldsmith and Wu 2008)
> (main reason for adopting identification) “increase advertising relevance".

#### FLOSS : Free Libre Open Source Software

* Richard Stallman: "free as in freedom" / Linus Torvalds (GNU / Linux, copyleft)  
  * Right to fork  🍴
  * Operating systems / software: Linux flavours (security, diversity, [personalization](http://hannahmontana.sourceforge.net/))

* Open Access (journals, etc)
  * [Open knowledge foundation](https://okfn.org/)
  > Knowledge is open if anyone is free to access, use, modify, and share it — subject, at most, to measures that preserve provenance and openness. (Open Definition [2.1](https://opendefinition.org/od/2.1/en/)) 

  * Case: Aaron Swartz (RSS, Markdown, CC, Reddit, and millions of "liberated" papers) [The Internet’s Own Boy: The Story of Aaron Swartz](https://www.youtube.com/watch?v=9vz06QO3UkQ)

  * Case: Sci-hub library (Alexandra Elbakyan) https://sci-hub.tw/
  
  * Code / Law

      * Lawrence Lessig (2006), Creative Commons: code is law, and an opportunity to ask for new regulators
      * Case: Tim Werners-Lee "Contract for the Web" (Goverments, Companies, Citizens)  https://9nrane41lq4966uwmljcfggv-wpengine.netdna-ssl.com/wp-content/uploads/Contract-for-the-Web.pdf

#### Architecture: decentralised/distributed protocols
* Diaspora https://diasporafoundation.org/
* DAT  https://dat.foundation/
* Blockchain-based (e.g. Scuttlebutt)  https://scuttlebutt.nz/    

* Obfuscation
    * VPNs (e.g. riseup) https://riseup.net/en/vpn/windows  
    * TOR (.onion protocol)  https://www.torproject.org/  
    * Electronic Frontier Foundation (e.g. panopticlick) https://panopticlick.eff.org/   

* Platforms and different economic models
    * Wikipedia / Wikidata 
    * Platform Cooperativism (vs "sharing" economy) (Scholtz) https://platform.coop/   

#### Activism / critical journalism
* Tactical visibility
    * Data breaches (the whistleblower persona): Edward Snowden, Chelsea Manning 
    * Wikileaks (Julian Assange) https://wikileaks.org/  

* Data activist research: Datactive (Milan et al) https://data-activism.net/ 
    * examine, manipulate, leverage, and exploit data to "fuel research for the social good"
    * use, mobilize, and creation of datasets for social causes
    * develop and employ data tech to frustrate massive data collection

* Propublica https://www.propublica.org/  
* Tactical Tech https://tacticaltech.org/#/  
* Sharelab https://labs.rs/en/  
>Collect , process and repurpose data to fuel research for the social good  

#### Short activity: 

Discuss (1-3 people):

1. Which of the former tactics do you believe is more adequate for the current data surveillance model, and why? New regulation? Obfuscation techniques? Data activism? Tactical Visibility?

2. Do you actively use one of these services/systems/platforms/etc? if not, why?

> the feeling of being 'creeped out' by algorithms as an essential part of acknowledging our vulnerability in this digitally databased world, and building on that feeling to try to understand some of these platform logics

## Recap MAP
Goals:
* Use digital tools to collect, analyse and present data
* Critically reflect on the production and use of data in specific cases

### Ethics
* researcher as data "processor" under GDPR
> collection, recording, organisation, structuring, storage, adaptation or alteration, retrieval, consultation, use, disclosure by transmission, dissemination or otherwise making available, alignment or combination, restriction, erasure or destruction

* personal data / sensitive data

### Data taxonomies / vocabulary
* Data is:
  *  Abstract, Discrete, Aggregative, Meaningful (regardless of format, medium, language, producer, and context) -- (Rosenberg 2013)
  * Taxonomical (relational / ordered) / Typological (primary, derived, metadata) / Genetic (is data regardless of its interpretability) -- (Floridi 2010)
  * Quantitative
      * Nominal
  	* Ordinal
  * Qualitative
  * Structure
  	* Structured
  	* Semi-structured
  	* Unstructured
  * Source
  	* Captured
  	* Exhaust
  	* Derived
  * Type
  	* Indexical
  	* Attribute
  	* Metadata
  * FAIR data: findable, accessible, interoperable, reusable (Wilkinson et al 2018)

### Tools
* Markdown
* Data Miner
* Raw
* Openrefine
* Voyant
* Instagrab
* Carto
* TCAT
* Gephi
* \* Pandas / Python / Jupyter-notebooks

### Methods
* Digital Methods ('native' digital objects) (Rogers)
* Document analysis
* Controversy analysis / issue mapping (Marres)
* Network analysis (Venturini)
* ANT: actors and community detection (Latour)
* Cultural analytics (Manovich)


MAP IMAGE (placeholder)


## Recommended Readings for the session

* Colakides, Yiannis. State Machines: Reflections and Actions at the Edge of Digital Citizenship, Finance, and Art. Amsterdam: Institute of Network Cultures, 2019. (Chapter: “There is no anonymity in the database”, 12 p.)
* Daly, Angela, Kate Devitt, and Monique Mann, eds. Good Data. Theory on Demand 29. Amsterdam: Institute of Network Cultures, 2019. (Chapter 14: Data for the Social Good: Toward a Data-Activist Research Agenda, 16 p.) 
* Reply All #112 The Prophet. Gimlet Media,  Accessed November 25, 2019. https://gimletmedia.com/shows/reply-all/j4hl36.


## Recommended readings

**I strongly recommend to take a look at the INC publications catalogue** (most of them are free): https://networkcultures.org/publications/  

* This Is Not an Atlas: A Global Collection of Counter-Cartographies. First edition. Sozial- Und Kulturgeographie, volume 26. Bielefeld: Transcript, 2018.
* Goldsmith, Jack, and Tim Wu. Who Controls the Internet?: Illusions of a Borderless World. New York: Oxford University Press, 2008.
* Gutiérrez, Miren, and Stefania Milan. “Playing with Data and Its Consequences.” First Monday 24, no. 1 (January 3, 2019).
* Hands, Joss. @ Is for Activism: Dissent, Resitance and Rebellion in a Digital Culture. London ; New York, NY: Pluto, 2011.
* Lessig, Lawrence. Code: And Other Laws of Cyberspace, Version 2.0. 2nd Revised ed. edition. New York: Basic Books, 2006.
* Scholz, Trebor. “Platform Cooperativism vs. the Sharing Economy.” Medium, July 10, 2015. https://medium.com/@trebors/platform-cooperativism-vs-the-sharing-economy-2ea737f1b5ad.
* Scholz, Trebor, and Nathan Schneider. Ours to Hack and to Own. New York, 2017.
* Stallman, Richard, Joshua Gay, and Free Software Foundation (Cambridge Mass.). Free Software, Free Society: Selected Essays of Richard M. Stallman. Free Software Foundation, 2002.

## Other Resources
* The Internet’s Own Boy: The Story of Aaron Swartz. Accessed November 20, 2019. http://www.imdb.com/title/tt3268458/.
