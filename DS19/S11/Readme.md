# Session 11

Block 5: Data Logic & Resistance Tactics

**SLIDES**: ([live](http://pablov.me/pres/ds19s11.html) / pdf)

## Plan for the day:

* Data contexts: Data Logic
* Jupyter notebooks + Pandas dataframes

### Data contexts: Data Logic

#### Discipline / control

* (Beniger 1986)
  * 20th century "societal control revolution" : communication tech & information processing adjusted to advancement of manufacturing & transportation of 19th century
  * control as the capability of one agent to influence another with a determined purpose 

* Case: Cybersin (Allende's socialist "internet")

<img src="cybersin.png" width="500">   

Cybersin project (prototype)

* Control after decentralization (Galloway 2004)

|Period|Machine|Diagram|Manager|
|------|-------|-------|-------|
|Sovereign Society|Simple mechanical machines|Centralization|Hierarchy|
|Disciplinary Society|Thermodynamic machines|Decentralization|Bureaucracy|
|Control Society|Cybernetic machines, computers|Distribution|Protocol|

> (protocol as a)  technique for achieving voluntary regulation within a contingent environment (...) Viewed as a whole, protocol is a distributed management system that allows control to exist within a heterogeneous material milieu

### Short Activity:
1. Select one of your media-relate daily activities (anything involving a digital devices, like the ones we listed on our surveillance class)
2. Identify points of control (where gate-keepig or decision-making of how data flows are made).
3. Is it a sovereign / disciplinary / control system? Who or what is managing it?

#### Computational Logic

* Codicillia: emperors orders
* Code as a medium for the transmission of power (Kittler 2008)

* Capture model (Agre 1991)
  * human activities can be framed as the sum of a set of unitary actions and the rules to compound these activities into sequences.
  * **Grammar**: not so much to the content of each activity, but to the architecture that allows human activities to be represented by computers

<img src="c-room.png" width="500">  

Chinese room problem (Searle 1980)

* The representation of information (the grammar), the intentions that guide the creation of the frameworks used with this grammar (the development), and the multiplication of methods in future epistemological directives, are already embedded in the many manifestations of the digital objects.

<img src="mosesbridge.jpg" width="500">   

Moses' Bridges

* Inherent politics of technology (Winner 1980): 
* ‘politics’ as arrangements of power and authority in human associations that include the
design and use of technological devices:

> rather than insist that we immediately reduce everything to the interplay of social forces, it suggests that we pay attention to the characteristics of technical objects and the meaning of those characteristics



* Case: faulty mobile phone sensors https://www.nytimes.com/2019/08/20/world/europe/denmark-cellphone-data-courts.html  
  * bug 1: file conversion error (leaves data out)  
  * bug 2: wrong linking to cellphone towers
  * Flaws apply to 10, 700 cases since 2012 

* Case: NYPD compstats "crime machine"

<img src="compstats.gif" width="500">   

> “The automatic machinery of a big factory is much more despotic than the small capitalists who employs workers ever have been” (Engels 1978, 731).


## Jupyter notebooks + Pandas dataframes

### Instructions:

1. Download the .ipynb file to an easy to access folder
2. Download the csv example file to the same folder
3. Open Anaconda Navigator
4. Launch Jupyter notebook
5. You will see a file structure from your computer
6. Navigate to your .ipynb  file to open it
7. After exploring the cells activation, try to open and modify another file of your choice
8. How does this computational frame (pandas) "solidify" (enhance the grammar of) a type of knowledge

**Notebook**: [DS19 Introductory data handling with Jupyter notebooks (.ipynb file)](DS19-intro.ipynb) (*right click -> save as...*)  
**csv example file**: [tcat_Ds19cc_nomos-20190917-20190921-greta-----------randomTweets-1000-1dfc4bf59a.csv](tcat_Ds19cc_nomos-20190917-20190921-greta-----------randomTweets-1000-1dfc4bf59a.csv) (*right click -> save as...*)  

## Mini project B5: 
This mini project focuses on exploring what is kept outside of your Research Project. 
You can use any of the tools/databases to exemplify this. Try to use (if applicable) the topics of this and previous blocks.
The main question that should be addressed by your fourth mini-project is:

***What not?***

What do you think is not answerable with your design? Are some parts of your question unanswerable? What is an obvious limitation of your design? What is "out" of data? What is missing from our overview? It missing because it was not included in our sources, because it can be easily adapted to our formats, because it was not evident, or because it is impossible to capture?

You don't have to address all of these questions, focus on the ones that are most relevant for your project perspective. You also don't have to be limited to this set of questions: what is out of data is usually quite vast, so feel free to expand them.

As before, the mini-project **will be presented next week during class. Groups presenting will be randomly chosen on Tuesday before noon. (but by now, you know you'll be presenting if you haven't done it in the last 4 blocks)** Some groups will also be chosen as replyers to comment on the work of your peers. **There is no need for any group to populate their wiki-projects for this last block. Instead, you may start to work on your written submission for the final project**.


## Required Readings (2)
a. Agre, Philip E. 1994. ‘Surveillance and Capture: Two Models of Privacy’. The Information Society 10 (2): 101–27.
b. Pasquinelli, Matteo, ed. Alleys of Your Mind. Lüneburg: meson press, 2015. (Chapter 1: Texeira, Ana, The pigeon in the machine, 14 p.)
c. Hear (podcast) Reply All, The Crime Machine ([part I](https://gimletmedia.com/shows/reply-all/o2hx34/) and [part II](https://gimletmedia.com/shows/reply-all/n8hwl7))

## Recommended readings

* Anderson, Chris. “The End of Theory: The Data Deluge Makes the Scientific Method Obsolete.” Wired, June 23, 2008. https://www.wired.com/2008/06/pb-theory/.
* Beniger, James R. The Control Revolution: Technological and Economic Origins of the Information Society. Cambridge, Mass. ; London: Harvard University Press, 1986.
* Bucher, Taina. If...Then: Algorithmic Power and Politics. Oxford Studies in Digital Politics. Oxford, New York: Oxford University Press, 2018. (Introduction, 18p.)
* Engels, Friedrich. “On Authority.” In Marx-Engels Reader, 2nd ed., 730–33. New York: W. W. Norton and Co, 1978.
* Galloway, Alexander R. Protocol: How Control Exists After Decentralization. MIT Press, 2004.
* Gerlitz, Carolin, and Anne Helmond. “The like Economy: Social Buttons and the Data-Intensive Web.” New Media & Society 15, no. 8 (December 1, 2013): 1348–65. (17 p.)
* Kittler, Friedrich A. “Code (or, How You Can Write Something Differently).” In Software Studies: A Lexicon, by Matthew Fuller. MIT Press, 2008.
* Medina, Eden. Cybernetic Revolutionaries: Technology and Politics in Allende’s Chile. MIT Press, 2014.
* Rouvroy, Antoinette. “The End(s) of Critique : Data-Behaviourism vs. Due-Process.” In Privacy, Due Process and the Computational Turn, edited by Mireille Hildebrandt, 1 edition. Abingdon; New York: Routledge, 2015.
* Searle, John R. “Minds, Brains, and Programs.” Behavioral and Brain Sciences 3, no. 3 (September 1980): 417–24.
* Wiener, Norbert. 1965. Cybernetics: Or, Control and Communication in the Animaland the Machine. 2d pbk. ed. The M.I.T. Paperback Series. Cambridge, Mass: M.I.T. Press.
* Winner, Langdon. 1980. ‘Do Artifacts Have Politics?’ Daedalus 109 (1): 121–136.

## Other Resources

* Podcast: Reply All "The crime machine I" Accessed July 13, 2019. https://gimletmedia.com/shows/reply-all/o2hx34/.
* Podcast: Reply All "The crime machine I" Accessed July 13, 2019. https://gimletmedia.com/shows/reply-all/n8hwl7.
