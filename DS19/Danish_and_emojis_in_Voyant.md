# Dealing with emojis and danish language in Voyant

Instructions video:

[![Video](/images/video_screenshot.png)](https://vimeo.com/363100912)

* You can find the file for [danish stopwords here](danske_stopwords.txt)
* Emoji analysis tool: http://labs.polsys.net/tools/textanalysis/  